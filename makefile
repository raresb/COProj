CC=javac
SOURCE=Main.java 
TESTS=FooTest.java BarTest.java

app: $(SOURCE)
	$(CC) $(SOURCE) $(TESTS)
	
.PHONY: clean

clean:
	rm -f *.exe *.out *.dll *.class

