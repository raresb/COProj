public class Main{
	private static ITest[] TESTS=null; //array to hold tests
	private static final int TEST_TIME=2000; // 2sec/test 
	public static void main(String[] args)
		throws InterruptedException{
		int cnt=args.length;
		if(cnt<1){
			System.err.println("Usage:<app> <test 1> ... <test n>");
			return;
		}
		TESTS=new ITest[cnt];
		
		/* Load tests */
		for(int i=0; i<cnt ; i++){
			try{
				Class testClass=Class.forName(args[i]);
				TESTS[i]=(ITest)testClass.newInstance();
			}catch(Exception E){
				System.err.println(
					"Error loading '"+args[i]+"':"+
					E.getMessage()
				);
				return;
			}
		}

		/* Run test */
		System.out.println("Running tests...");
		for(int i=0; i<cnt ; i++){
			System.out.println(">"+args[i]);
			Thread t=new Thread(TESTS[i]);
			t.start();
			try{
				Thread.sleep(TEST_TIME);
				t.interrupt();
			}catch(InterruptedException x){
			    //ignore	
			}
		}
		System.out.println("Done testing");
		System.out.printf("  Test  |Result\n");
		for(int i=0; i<cnt ; i++){
			System.out.printf("%8s|%6d\n",
				args[i],
				TESTS[i].getResults()
			);
		}
	}
}