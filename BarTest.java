public class BarTest implements ITest{
	
	private int circlesDrawn=0;
	private static final int FPS = 60;
	
	public int getResults(){
		return circlesDrawn;
	}
		
	public void run(){
		System.out.println("Running...");
		try{
			doCircles();
		}catch(InterruptedException x){
			//do nothing
		}
	}
	
	private void doCircles()
		throws InterruptedException{
		for(long frame=0;;frame++){
			long t1=System.nanoTime();
			if(Thread.interrupted())
				throw new InterruptedException();
			drawCircle((int)(10*Math.abs(Math.sin(frame/10)))+5);
			circlesDrawn++;
			long dt=(System.nanoTime()-t1)/1000;
			if((dt/1000)<(1000/FPS))
				Thread.sleep((1000/FPS)-(dt/1000));	
		}	
	}
	
	private void drawCircle(int r){
		for(int i=0; i<(2*r) ; i++){
			double dy=Math.pow((r-i)*1.8,2);
			for(int j=0; j<(2*r) ; j++){
				if(dy+Math.pow(j-r,2) <= Math.pow(r,2))
					System.out.print('*');
				else 
					System.out.print(' ');
			}
			System.out.print('\n');
		}
	}
}