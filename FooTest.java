public class FooTest implements ITest{
	private int i;
	
	public int getResults(){
		return i;
	}
	
	public void run(){
		System.out.println("Running...");
		try{
			calcSquares();
		}catch(InterruptedException x){
			System.out.println("Interruped at i="+i);
		}
	}
	
	private void calcSquares()
		throws InterruptedException{
		for(i=0;;i++){
			if(Thread.interrupted())
				throw new InterruptedException();
			System.out.println(i+"^2 = "+(i*i));
		}	
	}
	

}